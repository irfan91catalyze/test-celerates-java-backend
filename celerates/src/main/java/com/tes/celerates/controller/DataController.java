package com.tes.celerates.controller;

import com.tes.celerates.model.DataModel;
import com.tes.celerates.model.JurnalModel;
import com.tes.celerates.repository.DataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.text.NumberFormat;
import java.time.LocalDate;
import java.time.YearMonth;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@RestController
@RequestMapping("/datas")
public class DataController {

    @Autowired
    private DataRepository dataRepository;

    @GetMapping
    public List<DataModel> getDatas(){
        return  dataRepository.findAll();
    }

    @RequestMapping(value = "/jurnal", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody List<JurnalModel> jurnal() {
        LocalDate now = LocalDate.now();
        LocalDate lastDay = now.with(TemporalAdjusters.lastDayOfMonth());
        List<JurnalModel> list = new ArrayList<>();
        for (DataModel a : dataRepository.findAll()) {
            JurnalModel debit = new JurnalModel();
            JurnalModel kredit = new JurnalModel();
            debit.setWorkDay(a.getWilayahCode());
            debit.setNominalDebit(a.getAmount());
            debit.setNominalKredit(0);
            debit.setCoa(
                    a.getWilayahCode(),
                    a.getTransactionType(),
                    "debit",
                    a.getBankCode(),
                    a.getLob(),
                    a.getProductCode(),
                    a.getAgenCode()
            );
            debit.setJurnalDate(lastDay);

            kredit.setWorkDay(a.getWilayahCode());
            kredit.setNominalDebit(0);
            kredit.setNominalKredit(a.getAmount());
            kredit.setCoa(
                    a.getWilayahCode(),
                    a.getTransactionType(),
                    "kredit",
                    a.getBankCode(),
                    a.getLob(),
                    a.getProductCode(),
                    a.getAgenCode()
            );
            kredit.setJurnalDate(lastDay);

            list.add(debit);
            list.add(kredit);
        }
        return list;
    }

}
