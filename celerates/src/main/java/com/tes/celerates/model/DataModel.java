package com.tes.celerates.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "datas")
public class DataModel {

    @Id
    @Column(name = "id_transaksi" ,unique = true)
    private int transactionId;

    @Column(name = "kode_wilayah_kerja")
    private int wilayahCode;

    @Column(name = "kode_produk")
    private int productCode ;

    @Column(name = "kode_bank")
    private int bankCode;

    @Column(name = "lob")
    private int lob;

    @Column(name = "kode_agen")
    private int agenCode = 0;

    @Column(name = "jenis_transaksi")
    private int transactionType;

    @Column(name = "id_dd_reas", nullable = true)
    private Integer reasDdId;

    @Column(name = "nominal")
    private double amount;

    @JsonFormat(pattern = "yyyy-mm-dd")
    @Column(name = "created_date")
    private Date createdDate;

    @Column(name = "createdBy")
    private int createdBy;

}
