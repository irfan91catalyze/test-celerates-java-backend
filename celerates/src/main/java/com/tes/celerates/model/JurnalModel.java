package com.tes.celerates.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.time.LocalDate;
import java.util.Currency;
import java.util.Locale;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class JurnalModel {
   private String workDay;
   private String coa;
   private String nominalDebit;
   private String nominalKredit;
   private LocalDate jurnalDate;

    public void setCoa(
            int wilayahCode,
            int typeTransaction,
            String statusTransaction,
            int bankCode,
            int lobCode,
            int productCode,
            int agenCode
    ) {
        String segment1 = "0001" + wilayahCode + ".";
        String segment2 = wilayahCode + ".";
        String segment3 = "001.";
        String segment4 = setNaturalAccount(typeTransaction,statusTransaction);
        String segment5 = bankCode + ".";
        String segment6 = lobCode + productCode + ".";
        String segment7 = (agenCode != 0) ? String.valueOf(agenCode) : "000";

        this.coa = segment1 + segment2 + segment3 + segment4 + segment5 + segment6 + segment7;
    }

    public String setNaturalAccount(int typeTransaction, String statusTransaction) {
        String naturalAccount = "";
        if (typeTransaction == 1 && statusTransaction.equals("debit")) {
            naturalAccount = "12345ABC01.";
        } else if (typeTransaction == 1 && statusTransaction.equals("kredit") ) {
            naturalAccount = "12345CBA05.";
        } else if (typeTransaction == 2 && statusTransaction.equals("debit")) {
            naturalAccount = "54321DCBA01.";
        } else if (typeTransaction == 2 && statusTransaction.equals("kredit")) {
            naturalAccount = "54321ABCD05.";
        }
        return naturalAccount;
    }

    public void setNominalDebit(double nominalDebit) {
        if(nominalDebit != 0.0) {
            Locale localId = new Locale("in", "ID");
            DecimalFormat fmt = (DecimalFormat) NumberFormat.getCurrencyInstance(localId);
            String symbol = Currency.getInstance(localId).getSymbol(localId);
            fmt.setGroupingUsed(true);
            fmt.setPositivePrefix(symbol + " ");
            this.nominalDebit = fmt.format(nominalDebit);
        } else {
            this.nominalDebit = "";
        }
    }

    public void setNominalKredit(double nominalKredit) {
        if(nominalKredit != 0.0) {
            Locale localId = new Locale("in", "ID");
            DecimalFormat fmt = (DecimalFormat) NumberFormat.getCurrencyInstance(localId);
            String symbol = Currency.getInstance(localId).getSymbol(localId);
            fmt.setGroupingUsed(true);
            fmt.setPositivePrefix(symbol + " ");
            this.nominalKredit= fmt.format(nominalKredit);
        } else {
            this.nominalKredit = "";
        }
    }

    public void setWorkDay(int workDay) {
        if( workDay == 3){
            this.workDay = "Bandung";
        } else {
            this.workDay = "Kantor Pusat";
        }
    }
}
