package com.tes.celerates.repository;

import com.tes.celerates.model.DataModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DataRepository extends JpaRepository<DataModel,Long> {
}
